import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AdaptiveRaisedButton extends StatelessWidget {
  final String text;
  final Function handler;
  final Color color;
  final Color textColor;

  AdaptiveRaisedButton({this.text, this.handler, this.color, this.textColor});

  @override
  Widget build(BuildContext context) {
    return Platform.isIOS
        ? Center(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: CupertinoButton(
                child: Text(text),
                color: color,
                onPressed: handler,
              ),
            ),
          )
        : RaisedButton(
            child: Text(text),
            color: color,
            textColor: textColor,
            onPressed: handler,
          );
  }
}
