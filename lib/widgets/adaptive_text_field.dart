import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AdaptiveTextField extends StatelessWidget {
  final TextEditingController controller;
  final TextInputType keyboardType;
  final Function(String) onSubmitted;
  final String text;

  AdaptiveTextField({this.controller, this.onSubmitted, this.text, this.keyboardType = TextInputType.text });

  @override
  Widget build(BuildContext context) {
    return Platform.isIOS
        ? Container(
            margin: EdgeInsets.symmetric(horizontal: 0, vertical: 5),
            child: CupertinoTextField(
              placeholder: text,
              controller: controller,
              onSubmitted: onSubmitted,
              keyboardType: keyboardType,
            ),
          )
        : TextField(
            decoration: InputDecoration(labelText: text),
            controller: controller,
            onSubmitted: onSubmitted,
            keyboardType: keyboardType,
          );
    ;
  }
}
